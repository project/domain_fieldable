CONTENTS OF THIS FILE:
-----------------------

  * Description
  * Installation

Description
----------

 This module creates a single bundle "Domain Fieldable" entity based on Domains.

Installation
------------

 * Domain Fieldable depends on the domain and entity module, download and install it 
   from http://drupal.org/project/domain and http://drupal.org/project/entity
 * Copy the whole domain_fieldable directory to your modules directory
   (e.g. DRUPAL_ROOT/sites/all/modules) and activate the 
	 Domain Fieldable module.
 * Navigate to admin > structure > domain > manage fields to add/edit and delete fields.
 * Navigate to admin > structure > domain > edit domain to submit field values.
